# Aval 01 - Projeto
[![pipeline status](https://gitlab.com/anaruth/devmob-aval01/badges/master/pipeline.svg)](https://gitlab.com/anaruth/devmob-aval01/-/commits/master)

 * Faça fork do projeto base

- Implemente o projeto de modo que ele seja
  - Instalável no Dispositivo móvel
  - Funcione offline (cache, serviceworker)
  - Implemente os conceitos dispostos na sala
  - Deve ter tela de splash screen
  - Que seja um PWA
